# ejemplos de uso de archivos de tutorial en 
# https://www.youtube.com/watch?v=t8pPdKYpowI ,min 3:58
import openpyxl

inventory_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inventory_file["Sheet1"]

products_per_supplier = {}
total_value_per_supplier = {}

for product_row in range(2,product_list.max_row +1):
    supplier_name = product_list.cell(product_row,4).value
    inventory = product_list.cell(product_row,2).value
    supplier_value = product_list.cell(product_row,3).value
    inventory_price  = product_list.cell(product_row,5)

    if supplier_name in products_per_supplier:
        products_per_supplier[supplier_name] += 1
        total_value_per_supplier[supplier_name] = total_value_per_supplier[supplier_name] + supplier_value * inventory
    else:
        products_per_supplier[supplier_name] = 1
        total_value_per_supplier[supplier_name] = supplier_value * inventory
    
    inventory_price.value = supplier_value * inventory

inventory_file.save("inventory_w_inventory_value.xlsx")

print(products_per_supplier)
print(total_value_per_supplier)
        